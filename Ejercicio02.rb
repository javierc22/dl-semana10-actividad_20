module Formula
  def perimetro(*args)
    # Perímetro de cuadrado:
    return args[0] * 4 if args.size == 1
    # Perímetro de rectangulo:
    return (args[0] * 2) + (args[1] * 2) if args.size == 2
  end

  def area(*args)
    # Area de cuadrado:
    return args[0] * args[0] if args.size == 1
    # Area de rectangulo:
    return args[0] * args[1] if args.size == 2
  end
end

class Rectangulo
  include Formula
  attr_reader :base, :altura
  def initialize(base, altura)
    @base = base
    @altura = altura
  end

  def lados
    puts "Base: #{@base}, Altura: #{@altura}"
  end
end

class Cuadrado
  include Formula
  attr_reader :lado
  def initialize(lado)
    @lado = lado
  end

  def lados
    puts "Lados: #{@lado}"
  end
end

Rectangulo.new(2, 4).lados # => Base: 2, Altura: 4
Cuadrado.new(2).lados # => Lados: 2

cuadrado = Cuadrado.new(5)
cuadrado.lados # => Lados: 5
puts "Perímetro: #{cuadrado.perimetro(cuadrado.lado)}" # => Perímetro: 20
puts "Area: #{cuadrado.area(cuadrado.lado)}" # => Area: 25

rectangulo = Rectangulo.new(2, 5)
rectangulo.lados # => Base: 2, Altura: 5
puts "Perímetro: #{rectangulo.perimetro(rectangulo.base, rectangulo.altura)}" # => Perímetro: 14
puts "Area: #{rectangulo.area(rectangulo.base, rectangulo.altura)}" # => Area: 10

